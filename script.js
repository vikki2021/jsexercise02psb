import './styles.scss';
import { movies } from './src/data';

// console.log(movies);
// import $ from 'jquery';
// import 'bootstrap';
// $('body').append('jquery + bootstrap works!');
// every files in "static" folder can be used directly like that
// app.innerHTML += '<img src="images/kitten.jpg" style="width:100px;" />';
const app = document.getElementById('app');
const recentMovies = movies.filter((movie) => movie.year >= 2000);
// console.log(recentMovies);
const allMovieBtn = '<button type="button" class="btn btn-all">ALL</button>';
const recentMovieBtn = '<button type="button" class="btn btn-recent">RECENT FILM ONLY</button></header>';
// recent movie area--------------------------------------------
function renderRecent() {
  app.innerHTML = '';// first clean the html,super important!!!!!!
  let recentContainer = '<div class="card-columns flex-wrap">';
  let i = 0;// use for of, need to add id manually.
  for (const recentMovie of recentMovies) {
    recentContainer += `<div class="card" id="${i}" style="width: 18rem; height: 21rem;">`;
    const b = `${recentMovie.img}`;

    if (b === 'true') {
      recentContainer += `
    <img src="images/${recentMovie.imdb}.jpg" style="width:70%;" class="card-img-top " alt="${recentMovie.title}">
    </div>
 
  `;
    } else {
      recentContainer += `
  <div  class="card-img-top noimgcard ">${recentMovie.title}</div>
</div>
`;
    }
    i++;// increment i.
  }

  const endRecentContainer = '</div>';

  const recentTitle = '<header><h1 >Hackerflix</h1>';

  app.innerHTML += recentTitle + allMovieBtn + recentMovieBtn
  + recentContainer + endRecentContainer;

  let recentDetailContainer = '';
  let j = 0;// actually, let i= let j.
  for (const recentMovie of recentMovies) {
    recentDetailContainer += `
  <div class="card border-primary mb-3 detail-hidden" id="detail${j}" style="max-width: 18rem;">
  <div class="card-body" >
  
  <div class="card-body text-primary">
  <h5 class="card-title">${recentMovie.title}</h5>
  <p class="card-text">type:${recentMovie.genres}</p>
  <p class="card-text">year:${recentMovie.year}</p>
  <p class="card-text">rate:${recentMovie.note}</p>
  <p class="card-text">description:<br>${recentMovie.plot}</p>
  <a href="#" class="btn btn-primary recent-close">close</a>
  </div>
  </div>
  </div>
  `;
    j++;
  }
  app.innerHTML += recentDetailContainer;
}

// all movie area--------------------------------------------
function render() {
  app.innerHTML = '';// first clean the html,super important!!!!!!
  let cardContainer = '<div class="card-columns flex-wrap">';
  let i = 0;// use for of, need to add id manually.
  for (const movie of movies) {
    cardContainer += `<div class="card" id="${i}" style="width: 18rem; height: 21rem;">`;
    const b = `${movie.img}`;

    if (b === 'true') {
      cardContainer += `
  
    <img src="images/${movie.imdb}.jpg" style="width:70%;" class="card-img-top" alt="${movie.title}">
    </div>
 
  `;
    } else {
      cardContainer += `
  
  <div  class="card-img-top noimgcard">${movie.title}</div>
</div>


`;
    }
    i++;// increment i.
  }

  const endCardContainer = '</div>';

  const title = '<header><h1 >Hackerflix</h1>';

  app.innerHTML += title + allMovieBtn + recentMovieBtn + cardContainer + endCardContainer;

  let detailContainer = '';
  let j = 0;// actually, let i= let j.
  for (const movie of movies) {
    detailContainer += `
  <div class="card border-primary mb-3 detail-hidden" id="detail${j}" style="max-width: 18rem;">
  <div class="card-body" >
  
  <div class="card-body text-primary">
  <h5 class="card-title">${movie.title}</h5>
<p class="card-text">type:${movie.genres}</p>
<p class="card-text">year:${movie.year}</p>
<p class="card-text">rate:${movie.note}</p>
<p class="card-text">description:<br>${movie.plot}</p>
<a href="#" class="btn btn-primary close">close</a>
</div>
</div>
</div>
  `;
    j++;
  }
  app.innerHTML += detailContainer;
}

render();

// all movie addEventListener area------------------------------------------------------------
document.body.addEventListener('click', (e) => {
  if (e.target.parentNode.matches('.card')) {
    console.log(e.target.parentNode.id);
    const showDetail = document.getElementById(`detail${e.target.parentNode.id}`);
    showDetail.classList.remove('detail-hidden');
  } else if (e.target.matches('.close')) {
    // console.log(e.target);
    render();
  } else if (e.target.matches('.recent-close')) {
    // console.log(e.target);
    renderRecent();
  } else if (e.target.matches('.btn-recent')) {
    console.log(e.target);
    console.log('toto');
    renderRecent();
  } else if (e.target.matches('.btn-all')) {
    console.log('ttit');
    render();
  }
});
// click 'all' or 'only recent film' area-----------------------------------------------------------
// create two buttons:'all' and 'recent film only'.
// create a html area (with condition) to only display films after year 2000,and
// solution1?
// .hide it with scss
// give all films an id, #all${i}, films after 2000 an id,#recent${i}
// addEventListener to trigger the 'recent film only' btn, and add .hide class to #all${i},
// remove .hide
// class to #recent${i}.
// addEventListener to trigger the 'all' btn, and add .hide class to #all${i},
// remove .hide  class to #recent${i}.

// solution2?
// if the target matches btn-recent-->renderRecent()
// if the target matches btn-all-->render()
// renderRecent();
// document.body.addEventListener('click', (e) => {
//   if (e.target.matches('.btn-recent')) {
//     console.log(e.target);
//     console.log('toto');
//     renderRecent();
//   }
//   if (e.target.matches('.btn-all')) {
//     console.log('ttit');
//     render();
//   }
// });
