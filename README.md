**Exercises with Hackerflix(EN Version):**

**Hackerflix and chill**
**Basic guidelines**
**use our seed parcel**
- git clone https://github.com/ltruchot/parcel-seed-js-eslint-sass
- rename this folder "hackerflix", open it with VSCode, delete ".git" at the root
- open a terminal, then
- npm install
- npm start
- open the browser on http: // localhost: 1234
- clean the project
- uncomment the link to bootstrap in styles.scss
- remove mentions of jquery and bootstrap in script.js
- remove unnecessary html (but keep the div # app)
- forbidden to touch the HTML during the rest of the exercise: all the HTML appears in the div # app!

**Description of the challenge**
**Static javascript****

- We are making a site for fans of movies that talk about "hacking"
- The page is just a big mosaic of movie covers, much like Netflix but simpler
- use the data present in movies.js
- export them from a file
- import them into your script.js
- copy the posters folder to your static folder (here is a download link)
- all the "imbd posters" of the films are visible on your home page in a large "flex-wrap" container
- WARNING: some films do not have a "poster" (their img property is then false). In this case, a colored rectangle replaces the image, and the movie title is written in this rectangle.


**Dynamic javascript**
- clicking on a "poster" opens a popup: a centered div, above the elements, in fixed position
- This popup displays all the information available for the clicked movie
- This popup contains a "close" button: click on the cache again
- We can of course find this popup several times in a row, by clicking on different films
- There is a button at the top of the site: "Recent film only"
- Clicking on it hides all the covers of films made before the year 2000


**Bonus (not counted in the JS event)**
- A "select" field allows you to filter the films by genre: you choose your genre and you only see the films concerned.
- the first choice in this select is "see all", to be able to return to the initial state
- another select allows you to classify the films by "ratings", decreasing or increasing (the genre filter remains active)
- At the top of the site, a small carousel "highlights" 10 films drawn at random (3 visible, 7 invisible, which rotate in the carousel)
- All covers have a small "empty heart" icon: click on the filled one and vice versa (without opening the popup)
- "liking" films (by clicking on this heart) dynamically changes the content of the "highlights" of the carousel
- according to the user's tastes, only films of the same "genre" are offered in the carousel
- The state of the app is saved in the localStorage
- It's all beautiful, original, ergonomic and responsive


----------------------------------------------------------------------------------
# Parcel seed

> A very simple seed to start a modern Single Page App development in no time
> 
## how to use

First use:
- npm install
- npm start

Next use:
- npm start

Before final deploy:
- npm run build
- profit?

## ESlint

To fully enjoy ESlint, please
- install the official VSCode "ESlint" extension
- add this to your VSCode settings:
```json
  //...
  "editor.codeActionsOnSave": {
    "source.fixAll.eslint": true
  },
  //...
```

## JQuery and Bootsrap for quick POCs 

If needed, you can uncomment `bootstrap` + `jquery` imports in
- script.js
- style.scss
  
If you don't need them at all, you can remove thoses comments. Then in `package.json`:
  - remove deps
    - popper.js
    - bootstrap
    - jquery
